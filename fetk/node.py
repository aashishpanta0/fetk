import numpy as np


def factory(nodes):
    """Create objects required internally by fetk modules

    Parameters
    ----------
    nodes : list
        nodes[n][0] is the external node number `xn` of the nth node
        nodes[n][1:] are the coordinates of the ith node

    Returns
    -------
    coords : ndarray
        The nodal coordinates
    node_map : dict
        Mapping from external node number `xn` to internal number `n`.  That is
        `n = node_map[xn]` and `x = coords[node_map[xn]]` are the coordinates of node
        `xn`.

    Examples
    --------

    >>> nodes = [[1, 0, 0], [2, 0, 1], [5, 0, 4]]
    >>> x, m = fe_objects(nodes)
    >>> x
    np.array([[0.0, 0.0], [0.0, 1.0], [0.0, 4.0]])
    >>> m
    {1: 0, 2: 0, 5: 0}

    """
    node_map = {}
    dimension = 0
    coords = np.zeros((len(nodes), 3), dtype=float)
    for (n, defn) in enumerate(nodes):
        xn, *xc = defn
        dimension = max(len(xc), dimension)
        if dimension > 3:
            raise ValueError(f"Dimension of node {xn} > 3")
        coords[n, : len(xc)] = xc
        if xn in node_map:
            raise ValueError(f"Duplicate node ID {xn}")
        node_map[xn] = n
    if dimension > 3:
        raise ValueError("Coordinate dimension must be <= 3")
    coords = coords[:, :dimension]
    if len(coords.shape) == 1:
        coords = coords.reshape((coords.shape[0], 1))
    return coords, node_map
