import numpy as np
from functools import wraps
from typing import Any, Callable
from collections.abc import Sequence

scalar_types = (float, int, np.float, np.int)
string_types = (str,)


def is_sequence(a):
    return isinstance(a, Sequence) and not isinstance(a, string_types)


def fill_missing(a, *, length, missing=0.):
    x = np.full(length, missing)
    x[:len(a)] = a
    return x


class ClassPropertyContainer(object):
    """
    Allows creating a class level property (functionality for
    decorator).
    """

    def __init__(self, prop_get: Any, prop_set: Any = None):
        """
        Container that allows having a class property decorator.
        :param prop_get: Class property getter.
        :param prop_set: Class property setter.
        """
        self.prop_get: Any = prop_get
        self.prop_set: Any = prop_set

    def __get__(self, obj: Any, cls: type = None) -> Callable:
        """
        Get the property getter.
        :param obj: Instance of the class.
        :param cls: Type of the class.
        :return: Class property getter.
        """
        if cls is None:
            cls = type(obj)
        return self.prop_get.__get__(obj, cls)()

    def __set__(self, obj, value) -> Callable:
        """
        Get the property setter.
        :param obj: Instance of the class.
        :param value: A value to be set.
        :return: Class property setter.
        """
        if not self.prop_set:
            raise AttributeError("cannot set attribute")
        _type: type = type(obj)
        if _type == ClassPropertyMetaClass:
            _type = obj
        return self.prop_set.__get__(obj, _type)(value)

    def setter(self, func: Callable) -> "ClassPropertyContainer":
        """
        Allows creating setter in a property like way.
        :param func: Getter function.
        :return: Setter object for the decorator.
        """
        if not isinstance(func, (classmethod, staticmethod)):
            func = classmethod(func)
        self.prop_set = func
        return self


def classproperty(func):
    """
    Create a decorator for a class level property.
    :param func: This class method is decorated.
    :return: Modified class method behaving like a class property.
    """
    if not isinstance(func, (classmethod, staticmethod)):
        # The method must be a classmethod (or staticmethod)
        func = classmethod(func)
    return ClassPropertyContainer(func)


class ClassPropertyMetaClass(type):
    """
    Metaclass that allows creating a standard setter.
    """

    def __setattr__(self, key, value):
        """Overloads setter for class"""
        if key in self.__dict__:
            obj = self.__dict__.get(key)
        if obj and type(obj) is ClassPropertyContainer:
            return obj.__set__(self, value)

        return super(ClassPropertyMetaClass, self).__setattr__(key, value)


class ClassPropertiesMixin(metaclass=ClassPropertyMetaClass):
    """
    This mixin allows using class properties setter (getter works
    correctly even without this mixin)
    """

    pass


def split(string, *, sep, transform=lambda x: x, maxsplit=-1):
    return [transform(_.strip()) for _ in string.split(sep, maxsplit) if _.split()]


def remove_dups(container, sort=False):
    unique = list(set(container))
    if sort:
        unique.sort()
    return unique


def mutually_exclusive_args(*names, required=True):
    def decorator(fun):
        @wraps(fun)
        def inner(*args, **kwargs):
            kwds = dict([(name, kwargs.pop(name, None)) for name in names])
            num_defined = len([x for x in list(kwds.values()) if x is not None])
            keys = ", ".join(names)
            if num_defined > 1:
                raise MutuallyExclusiveArgumentError(
                    f"The keywords {keys} are mutually exclusive"
                )
            if required and not num_defined:
                raise MissingKeywordError(f"One of {keys} must be defined")
            for (key, value) in kwds.items():
                if value is not None:
                    kwargs[key] = value
            return fun(*args, **kwargs)


class MutuallyExclusiveArgumentError(Exception):
    pass


class MissingKeywordError(Exception):
    pass
