import numpy as np


def rectilinear2d(nx=1, ny=1, lx=1.0, ly=1.0):
    """Generate a 2D rectilinear mesh

    Parameters
    ----------
    nx, ny : int
        Number of elements in the x and y directions, respectively
    lx, ly : float
        Size of domain in the x and y directions, respectively

    Returns
    -------
    nodes : list
        nodes[n][0] is the node ID for the nth node
        nodes[n][1:] are the nodal coordinates of the nth node
    elements : list
        elements[e][0] is the element ID for the eth element
        elements[e][1:] are the node IDs for nodes defining the eth element

    Notes
    -----
    Both the node and element IDs are 1 based.  Eg, `nodes[0][0] = 1` and
    `nodes[0][n-1] = n`.  Likewise, `elements[0][0] = 1` and `elements[0][m-1] = m`.

    """
    if nx < 1 or ny < 1:
        raise ValueError("nx and ny must both be >= 1")
    if lx <= 0 or ly <= 0:
        raise ValueError("lx and ly must both be > 0")

    xp = np.linspace(0, lx, nx + 1)
    yp = np.linspace(0, ly, ny + 1)
    xc = np.array([(x, y) for y in yp for x in xp])
    shape = np.array([nx + 1, ny + 1], dtype=int)
    num_node = np.prod(shape)
    num_elem = np.prod(shape - 1)

    # Connectivity
    row = 0
    connect = np.zeros((num_elem, 4), dtype=int)
    for elem_num in range(num_elem):
        ii = elem_num + row
        elem_nodes = [ii, ii + 1, ii + nx + 2, ii + nx + 1]
        connect[elem_num, :] = elem_nodes
        if (elem_num + 1) % (nx) == 0:
            row += 1
        continue

    nodes = []
    for n in range(num_node):
        nodes.append([n + 1])
        nodes[-1].extend(xc[n])

    elements = []
    for e in range(num_elem):
        elements.append([e + 1])
        elements[-1].extend([n + 1 for n in connect[e]])

    return nodes, elements
