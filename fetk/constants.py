from types import SimpleNamespace


# Maximum number of dofs.  dofs are ordered:
# 1: ux (displacement x)
# 2: uy (displacement y)
# 3: uz (displacement z)
# 4: tx (rotation x)
# 5: ty (rotation y)
# 6: tz (rotation z)
# 7: T (temperature)
max_dof = 7

# Units
# (J, F, L, t, T)
# Load types
labels = SimpleNamespace(
    body_force=10,  # (0, 1, -3, 0, 0)
    gravity=11,  # (0, 0, 1, -2, 0)
    pressure=12,  # (0, 1, -2, 0, 0)
    #  Heat transfer
    body_flux=20,  # (1, 0, -3, -1, 0)
    surface_flux=21,  # (1, 0, -2, -1, 0)
    surface_film=22,  # (1, 0, -2, -1, -1)
)
