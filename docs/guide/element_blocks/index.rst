.. _eb_index:

==============
Element blocks
==============

For efficient storage and to minimize I/O, elements in the finite element model are grouped into element blocks.

.. rubric:: Contents

.. toctree::
   :maxdepth: 1

   description
   defining
   internal
   input
