======================
Internal represenation
======================

Internally, materials are represented by a ``fetk.material`` object defined by its model name, material integer id, parameters, and (optionally) name.  The material can be created using the ``fetk.material.factory`` method.


.. rubric:: Example

Define a linear elastic material model with Young's modulus :math:`E=10` and Poisson's ratio :math:`\nu=.25`.

.. code-block:: python

    >>> import fetk.material
    >>> material = fetk.material.factory("elastic", 1, {"E": 10, "nu": .25})
